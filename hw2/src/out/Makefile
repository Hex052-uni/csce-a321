# Eli Faso-Formoso
# 2020-07-19
# Makefile
# For CSCE A321 - Operating Systems

CFLAGS:=-Wall $(shell pkg-config fuse --libs)
CPPFLAGS:=-iquote=$(abspath ..) $(shell pkg-config fuse --cflags)# includes the parent direcotry
CC=gcc
objdir=$(abspath obj)

# To compile .tex to .pdf
texoutdir=$(abspath tex)
TEX=latex
TEXFLAGS=-output-directory=$(texoutdir) --shell-escape -interaction=nonstopmode
DVI=dvips
PS=ps2pdf


.PHONY: default all debug
default: CFLAGS+=-O0
default: all

debug: CPPFLAGS+=-DDEBUG
debug: CFLAGS+=-g -O0
debug: all

all: myfs report

myfs: $(objdir)/myfs.o $(objdir)/implementation.o
	$(CC) -o $@ $^ $(CFLAGS) $(CPPFLAGS)

$(objdir)/myfs.o: ../myfs.c | $(objdir)/
	$(CC) -c -o $@ $^ $(CFLAGS) $(CPPFLAGS)

$(objdir)/implementation.o: ../implementation.c | $(objdir)/
	$(CC) -c -o $@ $^ $(CFLAGS) $(CPPFLAGS)

$(objdir)/:
	mkdir $(objdir)


# Report from report.tex
.PHONY: report report-unavailable-as-missing-programs
report: $(shell which $(TEX) $(DVI) $(PS) > /dev/null && echo report.pdf || echo report-unavailable-as-missing-programs)

report-unavailable-as-missing-programs:
	@tput setaf 1 # color red
	@echo "Not attempting to construct report for the following reason(s):"
	@which $(TEX) > /dev/null || echo Cannot find $(TEX) >&2
	@which $(DVI) > /dev/null || echo Cannot find $(DVI) >&2
	@which $(PS) > /dev/null || echo Cannot find $(PS) >&2
	@tput sgr0 # reset color to default

report.pdf: $(texoutdir)/report.ps | $(texoutdir)/
	$(PS) $^ $@

$(texoutdir)/report.ps: $(texoutdir)/report.dvi | $(texoutdir)/
	# directing output to /dev/null because this command is super noisy
	$(DVI) -o $@ $^  2> /dev/null

$(texoutdir)/report.dvi: ../report.tex | $(texoutdir)/
	@# not using variable $^ here because of change of directory
	# directing output to /dev/null because this command is super noisy
	cd .. && $(TEX) $(TEXFLAGS) report.tex > /dev/null

$(texoutdir)/:
	mkdir $(texoutdir)


.PHONY: clean
clean:
	rm -f myfs
	rm -rf $(objdir)
	rm -rf $(texoutdir)
	rm -f report.pdf
