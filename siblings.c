#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>

// runs close and exits if error
void closeExit(int fd){
  if (close(fd) < 0) {
    fprintf(stderr, "error while closing file descriptor: %s\n", strerror(errno));
    exit(1);
  }
}

// runs write and exits if error
void writeExit(int fd, const void *buf, size_t count){
  if (write(fd, buf, count) < 0) {
    fprintf(stderr, "error while writing: %s\n", strerror(errno));
    exit(1);
  }
}

// runs read and exits if error
void readExit(int fd, void *buf, size_t count){
  if (read(fd, buf, count) < 0) {
    fprintf(stderr, "error while reading: %s\n", strerror(errno));
    exit(1);
  }
}

int main(int argc, char **argv) {
  int pipeAB[2];
  int pipeBC[2];
  int pipeCD[2];
  int pipeDA[2];

  if (pipe(pipeAB) == -1) {
    fprintf(stderr, "pipeAB could not be initialized: %s\n", strerror(errno));
    return 1;
  };
  if (pipe(pipeBC) == -1) {
    fprintf(stderr, "pipeBC could not be initialized: %s\n", strerror(errno));
    return 1;
  };
  if (pipe(pipeCD) == -1) {
    fprintf(stderr, "pipeCD could not be initialized: %s\n", strerror(errno));
    return 1;
  };
  if (pipe(pipeDA) == -1) {
    fprintf(stderr, "pipeDA could not be initialized: %s\n", strerror(errno));
    return 1;
  };
  

  int val = 39;

  pid_t childA = fork();
  if (childA == -1) {
    fprintf(stderr, "childA could not be initialized: %s\n", strerror(errno));
    return 1;
  };
  if (childA == 0) {
    closeExit(pipeAB[0]);
    closeExit(pipeBC[0]);
    closeExit(pipeBC[1]);
    closeExit(pipeCD[0]);
    closeExit(pipeCD[1]);
    closeExit(pipeDA[1]);
    // writing to pipeAB then reading from pipe DA
    writeExit(pipeAB[1], &val, sizeof(int));
    closeExit(pipeAB[1]);
    readExit(pipeDA[0], &val, sizeof(int));
    closeExit(pipeDA[0]);
    printf("A: %d\n", val);
    exit(0);
  }

  pid_t childB = fork();
  if (childB == -1) {
    fprintf(stderr, "childB could not be initialized: %s\n", strerror(errno));
    return 1;
  };
  if (childB == 0) {
    closeExit(pipeAB[1]);
    closeExit(pipeBC[0]);
    closeExit(pipeDA[0]);
    closeExit(pipeCD[0]);
    closeExit(pipeCD[1]);
    closeExit(pipeDA[1]);
    // reading from pipeAB then writing to pipe BC
    readExit(pipeAB[0], &val, sizeof(int));
    closeExit(pipeAB[0]);
    
    val++;
    
    writeExit(pipeBC[1], &val, sizeof(int));
    closeExit(pipeBC[1]);
    exit(0);
  }

  pid_t childC = fork();
  if (childC == -1) {
    fprintf(stderr, "childC could not be initialized: %s\n", strerror(errno));
    return 1;
  };
  if (childC == 0) {
    closeExit(pipeAB[0]);
    closeExit(pipeAB[1]);
    closeExit(pipeBC[1]);
    closeExit(pipeCD[0]);
    closeExit(pipeDA[0]);
    closeExit(pipeDA[1]);
    // reading from pipeBC and writing to pipe CD
    readExit(pipeBC[0], &val, sizeof(int));
    closeExit(pipeBC[0]);
    
    val++;
    
    writeExit(pipeCD[1], &val, sizeof(int));
    closeExit(pipeCD[1]);
    exit(0);
  }
  
  pid_t childD = fork();
  if (childD == -1) {
    fprintf(stderr, "childD could not be initialized: %s\n", strerror(errno));
    return 1;
  };
  if (childD == 0) {
    closeExit(pipeAB[0]);
    closeExit(pipeBC[0]);
    closeExit(pipeBC[1]);
    closeExit(pipeAB[1]);
    closeExit(pipeCD[1]);
    closeExit(pipeDA[0]);
    // reading from pipeCD and writing to pipe DA
    readExit(pipeCD[0], &val, sizeof(int));
    closeExit(pipeCD[0]);
    
    val++;

    writeExit(pipeDA[1], &val, sizeof(int));
    closeExit(pipeDA[1]);
    exit(0);
  }
  
  wait(NULL);
  exit(0);
  
}


