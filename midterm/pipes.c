#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <stdio.h>
#include <stddef.h>
#include <sys/wait.h>

void
closepipes(int pipefd[4][2]);
void
close_writerr(int fd);

int
childA(int pipefd[4][2]);
int
childB(int pipefd[4][2]);
int
childC(int pipefd[4][2]);
int
childD(int pipefd[4][2]);

int
main(int argc, char const *argv[]) {
	int proc_return_code = 0;
	int pipefd[4][2];

	//Create pipes
	for (size_t i = 0; i < 4; i++) {
		if (pipe(pipefd[i]) == -1) {
			// Error occured
			fprintf(stderr, "Pipe create error: %s\n", strerror(errno));

			_exit(1);
		}
	}

	int pida = 0, pidb = 0, pidc = 0, pidd = 0;
	int status;

	pida = fork();
	if (pida == 0) {
		proc_return_code = childA(pipefd);
	}
	else if (pida == -1) {
		//Close all pipes
		closepipes(pipefd);
		proc_return_code = 1;
	}
	else {
		//Parent
		pidb = fork();
		if (pidb == 0) {
			proc_return_code = childB(pipefd);
		}
		else if (pidb == -1) {
			// B would have used pipefd[1][0] and pipefd[2][1]
			close_writerr(pipefd[0][0]);
			close_writerr(pipefd[1][1]);
			// C would have used pipefd[1][0] and pipefd[2][1]
			close_writerr(pipefd[1][0]);
			close_writerr(pipefd[2][1]);
			// D would have used pipefd[2][0] and pipefd[3][1]
			close_writerr(pipefd[2][0]);
			close_writerr(pipefd[3][1]);
			proc_return_code = 1;
		}
		else {
			//Parent
			pidc = fork();
			if (pidc == 0) {
				proc_return_code = childC(pipefd);
			}
			else if (pidc == -1) {
				// C would have used pipefd[1][0] and pipefd[2][1]
				close_writerr(pipefd[1][0]);
				close_writerr(pipefd[2][1]);
				// D would have used pipefd[2][0] and pipefd[3][1]
				close_writerr(pipefd[2][0]);
				close_writerr(pipefd[3][1]);
				proc_return_code = 1;
			}
			else {
				//Parent
				pidd = fork();
				if (pidd == 0) {
					proc_return_code = childD(pipefd);
				}
				else if (pidd == -1) {
					//Close all unused pipes
					// D would have used pipefd[2][0] and pipefd[3][1]
					close_writerr(pipefd[2][0]);
					close_writerr(pipefd[3][1]);
					proc_return_code = 1;
				}
				else {
					//Parent
					waitpid(pidd, &status, 0);
					if (status > 0) {
						proc_return_code = status;
					}
				}
				waitpid(pidc, &status, 0);
				if (status > 0) {
					proc_return_code = status;
				}
			}
			waitpid(pidb, &status, 0);
			if (status > 0) {
				proc_return_code = status;
			}
		}
		waitpid(pida, &status, 0);
		if (status > 0) {
			proc_return_code = status;
		}
	}

	return proc_return_code;
}

void
closepipes(int pipefd[4][2]) {
	for (size_t i = 0; i < 4; i++) {
		if (close(pipefd[i][0]) == -1) {
			// Error occured
			fprintf(stderr, "Pipe close error: %s\n", strerror(errno));
		}
		if (close(pipefd[i][1]) == -1) {
			// Error occured
			fprintf(stderr, "Pipe close error: %s\n", strerror(errno));
		}
	}
}

void
close_writerr(int fd) {
	if (close(fd) == -1) {
		// Error occured, but file already closed.
		fprintf(stderr, "Pipe close error: %s\n", strerror(errno));
	}
}

int
childA(int pipefd[4][2]) {
	int proc_return_code = 0;
	int syscall_return;

	//Close all unused pipes
	close_writerr(pipefd[0][0]);
	close_writerr(pipefd[1][0]);
	close_writerr(pipefd[1][1]);
	close_writerr(pipefd[2][0]);
	close_writerr(pipefd[2][1]);
	close_writerr(pipefd[3][1]);

	int num = 39;
	syscall_return = write(pipefd[0][1], &num, sizeof(int));
	if (syscall_return == -1) {
		fprintf(stderr, "Write error: %s\n", strerror(errno));
		proc_return_code = 1;
	}
	close_writerr(pipefd[0][1]);

	if (syscall_return != -1) {
		syscall_return = read(pipefd[3][0], &num, sizeof(int));
		if (syscall_return == -1) {
			fprintf(stderr, "Read error: %s\n", strerror(errno));
			proc_return_code = 1;
		}
		else {
			printf("%d\n", num);
		}
	}
	close_writerr(pipefd[3][0]);

	return proc_return_code;
}

int
childB(int pipefd[4][2]) {
	int proc_return_code = 0;
	int syscall_return;

	//Close all unused pipes
	close_writerr(pipefd[0][1]);
	close_writerr(pipefd[1][0]);
	close_writerr(pipefd[2][0]);
	close_writerr(pipefd[2][1]);
	close_writerr(pipefd[3][0]);
	close_writerr(pipefd[3][1]);

	int num;
	syscall_return = read(pipefd[0][0], &num, sizeof(int));
	if (syscall_return == -1) {
		fprintf(stderr, "Read error: %s\n", strerror(errno));
		proc_return_code = 1;
	}
	close_writerr(pipefd[0][0]);

	if (syscall_return != -1) {
		num += 1;
		syscall_return = write(pipefd[1][1], &num, sizeof(int));
		if (syscall_return == -1) {
			fprintf(stderr, "Write error: %s\n", strerror(errno));
			proc_return_code = 1;
		}
	}
	close_writerr(pipefd[1][1]);

	return proc_return_code;
}

int
childC(int pipefd[4][2]) {
	int proc_return_code = 0;
	int syscall_return;

	//Close all unused pipes
	close_writerr(pipefd[0][0]);
	close_writerr(pipefd[0][1]);
	close_writerr(pipefd[1][1]);
	close_writerr(pipefd[2][0]);
	close_writerr(pipefd[3][0]);
	close_writerr(pipefd[3][1]);

	int num;
	syscall_return = read(pipefd[1][0], &num, sizeof(int));
	if (syscall_return == -1) {
		fprintf(stderr, "Read error: %s\n", strerror(errno));
		proc_return_code = 1;
	}
	close_writerr(pipefd[1][0]);

	if (syscall_return != -1) {
		num += 1;
		syscall_return = write(pipefd[2][1], &num, sizeof(int));
		if (syscall_return == -1) {
			fprintf(stderr, "Write error: %s\n", strerror(errno));
			proc_return_code = 1;
		}
	}
	close_writerr(pipefd[2][1]);

	return proc_return_code;
}

int
childD(int pipefd[4][2]) {
	int proc_return_code = 0;
	int syscall_return;

	//Close all unused pipes
	close_writerr(pipefd[0][0]);
	close_writerr(pipefd[0][1]);
	close_writerr(pipefd[1][0]);
	close_writerr(pipefd[1][1]);
	close_writerr(pipefd[2][1]);
	close_writerr(pipefd[3][0]);

	int num;
	syscall_return = read(pipefd[2][0], &num, sizeof(int));
	if (syscall_return == -1) {
		fprintf(stderr, "Read error: %s\n", strerror(errno));
		proc_return_code = 1;
	}
	close_writerr(pipefd[2][0]);

	if (syscall_return != -1) {
		num += 1;
		syscall_return = write(pipefd[3][1], &num, sizeof(int));
		if (syscall_return == -1) {
			fprintf(stderr, "Write error: %s\n", strerror(errno));
			proc_return_code = 1;
		}
	}
	close_writerr(pipefd[3][1]);

	return proc_return_code;
}
