# Compilation

The file [out/Makefile](out/Makefile) includes instructions to automatically
build head and tail. These can be built by running `make head` or `make tail`
(`make` / `make default` will build all of them).
`make debug` builds all suitable for debugging.

The report may be converted to pdf from latex by running `make report`
(a phony target for `report.pdf` which will warn if the required
programs to do so are unavailable). This is included in `make`/`make default`.
The reason this is done is so `make` will build the c source files correctly,
but the grader might not have the texlive package installed.

The file path to `nanpa` is hardcoded into the file, but may be changed by
defining an environment variable `NANPA_FILE` to be the path to it.
For example, running `NANPA_FILE=~clauter/nanpa make`.

All files may be cleaned up with `make clean`.
