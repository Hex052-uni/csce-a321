#!/bin/sh
# Eli Faso-Formoso
# 2020-06-31
# findlocation.sh
# For CSCE A321 - Operating Systems


# dash does not have the <<< operator, this script would be much shorter if it did.

grep -qE '^[0-9]{6}$' <<EOF || exit 1
${1}
EOF
if ! place="$( grep -oE "${1}"'[[:alnum:] ]+[[:alnum:]]' nanpa )" ; then
	# executed if grep fails
	exit 2
fi
cut -c7- <<EOF
${place}
EOF
