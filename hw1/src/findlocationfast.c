// Austin Williams
// findlocationfast.c
// CSCE A321 - Operating Systems
// 8 July 2020

#include <unistd.h> // for read() and write()
#include <sys/mman.h> // for mmap() and munmap()
#include <fcntl.h> // for open() and close()
#include <errno.h> // for errno
#include <string.h> // for strerror()

#include "shared.h" // for write_str()

#if !defined(NANPA_FILE)
#define NANPA_FILE "./nanpa"
#endif

// file is saved in memory line by line rather than character by character
struct Lines {
  char number[6];
  char location[25];
  char newline;
};


// checks if the string passed to argv[1] is 6 characters and composed of digits
int is6Digits(char *digits) {
  int length = 0;
  for (char* p = digits; *p; p++) {
    if (*p < '0' || *p > '9') {
      return 0;
    }
    length = length + 1;
  }
  if (length != 6) {
    return 0;
  }
  return 1;
}


// opens the file
int openFile(char *str) {
  int fd = open(str, O_RDONLY);
  if (fd < 0) {
    if (write_str(2, NANPA_FILE" file not found in current directory: ", NULL) || !write_str(2, strerror(errno), NULL)) {
      write_str(2, "\n", NULL);
    }
    _exit(1);
  }
  return fd;
}


// gets the size of file
off_t getSizeOfFile(int fd) {
  off_t offset;
  offset = lseek(fd, 0, SEEK_END);
  if (offset < 0) {
    if (write_str(2, "Could not get size of specified file: ", NULL) || !write_str(2, strerror(errno), NULL)) {
      write_str(2, "\n", NULL);
    }
    _exit(1);
  }
  return offset;
}


// uses mmap to save the file in memory
void* putFileIntoMem(int fd, size_t length) {
  void *ptr = mmap(NULL, length, PROT_READ, MAP_SHARED, fd, 0);
  if (ptr == MAP_FAILED) {
    if (write_str(2, "Could not mmap specified file: ", NULL) || !write_str(2, strerror(errno), NULL)) {
      write_str(2, "\n", NULL);
    }
    _exit(1);
  }
  return ptr;
}


// uses munmap to free the saved file in memory
int closeMemFile(void* fileInMem, size_t size) {
  int res = munmap(fileInMem, size);
  if (res == -1) {
    if (write_str(2, "Could not get munmap of specified file: ", NULL) || !write_str(2, strerror(errno), NULL)) {
      write_str(2, "\n", NULL);
    }
    _exit(1);
  }
  return res;
}


// closes the file
void closeFile(int fd) {
  if (close(fd) < 0) {
    if (write_str(2, "Could not close specified file: ", NULL) || !write_str(2, strerror(errno), NULL)) {
      write_str(2, "\n", NULL);
    }
    _exit(1);
  }
}


//compares the 6 digit strings: returns -1 if less than, 0 if equal to, or 1 if greater than
int isGreaterThan(char* number1, char* number2, int digits) {
  for (int i = 0; i < digits; i++) {
    if (number1[i] > number2[i]) {
      return 1;
    }
    if (number1[i] < number2[i]) {
      return -1;
    }
  }
  return 0;
}


// finds the line corresponding to the user-inputted 6 digit string 
int binarySearch(struct Lines* fileInMem, size_t left, size_t right, char* sixDigits) {
  if (right >= left) {
    int mid = left + (right-left) / 2;
    
    char *number = (fileInMem+mid)->number;
    int isGreater = isGreaterThan(number, sixDigits, 6);
    
    if (isGreater == 0) {
      return mid;
    }

    else if (isGreater == 1) {
      return binarySearch(fileInMem, left, mid-1, sixDigits);
    }
    else {
      return binarySearch(fileInMem, mid+1, right, sixDigits);
    }
  }
  return -1;
}


// prints the phone number's corresponding location
void printLocation(int offset, struct Lines *fileInMem) {
  // gets the length of just the location without trailing spaces
  size_t len = 25;
  for (; len && fileInMem[offset].location[len - 1] == ' '; len--);

  // prints the location name followed by a newline char and reports if error
  if (retry_write(1, fileInMem[offset].location, len, NULL) == -1) {
    if (write_str(2, "Write error: ", NULL) || !write_str(2, strerror(errno), NULL)) {
	write_str(2, "\n", NULL);
    }
    _exit(1);
  }
  write_str(1, "\n", NULL);
}


// main program
int main(int argc, char **argv) {
  // Check Usage
  if (argc != 2) {
    write_str(2, "Usage: findlocationfast [6 digit number]\n", NULL);
    return 1;
  }
  if (!is6Digits(argv[1])) {
    write_str(2, "Usage: findlocationfast [6 digit number]\n", NULL);
    return 1;
  }
  if (isGreaterThan("201200", argv[1], 6) > 0) {
    write_str(2, "[6 digit number] must be at least 201200\n", NULL);
    return 1;
  }
  
  // open file, save it in memory, close file
  int fd = openFile(NANPA_FILE);
  off_t size = getSizeOfFile(fd);
  void *ptr = putFileIntoMem(fd, (size_t) size);
  closeFile(fd);

  // make sure the file is divisible by 32 to use with struct Lines
  if (size%32 != 0) {
    write_str(2, "file is not divisible by 32", NULL);
    return 1;
  }
  
  // search for the 6 digit string using binary search
  int offset = binarySearch((struct Lines*) ptr, 0, size/32 - 1, argv[1]);
  if (offset == -1) {
    write_str(2, "Phone number is not in file\n", NULL);
    return 1;
  }
  else {
    // if found then print it
    printLocation(offset, (struct Lines*) ptr);
  }

  // frees the memory
  closeMemFile(ptr, size);
  return 0;
}
