// Eli Faso-Formoso
// 2020-06-31
// shared.h
// For CSCE A321 - Operating Systems


#ifndef PARAM_PARSE_HEADER
#define PARAM_PARSE_HEADER

#include <sys/types.h>

//! Parses the filename and length requested from the argumet string.
//! Exits on error.
//! Used for both head/tail, can't really imagine it'd be useful anywhere else.
void
parse_param(const char **argv, size_t *num, const char **filename);

//! Writes a string to fd. Number of characters written returned in *written.
//! written may be NULL.
//! Returns: -1 on error (errno set), 0 otherwise
int
write_str(int fd, const char *str, ssize_t *written);

//! Functions exactly like write, but retries if interrupted.
//! written may be NULL.
//! Returns: -1 on error (errno set), 0 otherwise
int
retry_write(int fd, const void *buf, size_t count, ssize_t *written);

//! Writes the error message from errno to stderr,
//! then calls _exit(1)
void
write_error_exit(int errno);

#endif
