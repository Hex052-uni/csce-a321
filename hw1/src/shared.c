// Eli Faso-Formoso
// 2020-06-31
// shared.c
// For CSCE A321 - Operating Systems


#include "shared.h"

#include <string.h> //for strerror()
#include <errno.h>
#include <unistd.h>

void
parse_param(const char **argv, size_t *num, const char **filename) {
	// Not checking for errors from write_str, because what am I going to
	// do if it fails? Write a message to stderr and exit?
	for (size_t i = 1; argv[i] != NULL; i++) {
		if (argv[i][0] == '\0') {
			continue;
			//I don't even know if this is possible, but figured I should check
		}
		if (argv[i][0] == '-') {
			if (argv[i][1] == 'n') {
				// head from coreutils uses whatever the last is, so that's
				// what this will do too.
				*num = 0;
				const char *start; //!< Start of number of lines
				if (argv[i][2] == '\0') {
					start = argv[++i];
					if (start == NULL || *start == '\0') {
						write_str(2, "Must have a number of lines after -n\n", NULL);
						_exit(1);
					}
				}
				else {
					//offset start after -n
					 start = argv[i] + 2;
				}

				do {
					*num = *num * 10 + (size_t)(*start - '0');
					// can be a do-while since we already know *start != '\0'
					if (*start < '0' || *start > '9') {
						if (write_str(2, "Number of lines '", NULL) || !write_str(2, argv[i], NULL)) {
							// Stop lines from being printed if the previous didn't
							write_str(2, "' contained non-number character\n", NULL);
						}
						_exit(1);
					}
					start++;
				} while (*start != '\0');
			}
			else  {
				// Must have an option.
				if (write_str(2, "Unknown option '", NULL) || !write_str(2, argv[i], NULL)) {
					// Stop lines from being printed if the previous didn't
					write_str(2, "'\n", NULL);
				}
				_exit(1);
			}
		}
		else {
			//Have a file name.
			if (*filename != NULL) {
				//Already had a file, can't have two
				write_str(2, "Cannot have two files to read from.\n", NULL);
				_exit(1);
			}
			*filename = argv[i];
		}
	}

	return;
}

int
write_str(int fd, const char *str, ssize_t *written) {
	size_t len = 0;
	for (const char *ptr = str; *ptr; ptr++, len++); // string length
	return retry_write(2, str, len, written);
}

int
retry_write(int fd, const void *buf, size_t count, ssize_t *written) {
	size_t remain = count;
	ssize_t total;
	if (count == 0) {
		if (written != NULL) {
			*written = 0;
		}
	}

	// Loop until there's an error that isn't EINTR or count characters
	// are written
	do {
		total = write(fd, buf, remain);
		if (total == -1) {
			if (errno == EINTR) {
				// interrupted, retry.
				continue;
			}
			// Actual error
			if (written != NULL) {
				*written = count - remain;
			}
			return -1;
		}
		remain -= total;
		buf += total;
	} while (remain != 0);
	if (written != NULL) {
		*written = count;
	}
	return 0;
}

void
write_error_exit(int errnum) {
	char *err = strerror(errnum);
	if (err != NULL) {
		write_str(2, err, NULL);
		// Not checking this error, because what am I going to do if it fails?
		// Write another message to stderr and exit?
	}
	_exit(1);
}
