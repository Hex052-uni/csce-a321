// Eli Faso-Formoso
// 2020-06-31
// tail.c
// For CSCE A321 - Operating Systems

#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h> //malloc, free
#include <errno.h>
#include <string.h> //for strerror only

#include "shared.h"

#define BUFFER_SIZE 256

typedef struct {
	char *line;
	size_t len;
	size_t allocated;
} line_t;


// FUNCTION DECLARATIONS

//! Reads the next line to and including delim.
//! If line is NULL, then -1 will be returned, but errno will not
//! indicate why. Don't do this.
//! line should have allocated == 0 or line == NULL if no memory is yet
//! allocated for the line.
//! Will append to line.
//! If an error occured partway though, the line that was retrieved
//! until the error is returned.
//! Retries on EINTR (if successful, returns 0 as normal).
//! on error, errno set approprately and will return -1.
//! Could be any errno from read or malloc (ENOMEM).
int
nextline(int fd, const char delim, line_t *line);

//! Frees the line. Does not fail.
inline void
freeline(line_t *line);


//BEGIN

int
main(int argc, char const *argv[]) {
	size_t num = 10; //!< Number of lines left to read
	int fd = 0; //!< Where to read from
	const char *filename = NULL; //!< The file to open and read from
	char delim = '\n'; //!< The delimiter. `man head` says may be \0
	line_t *buf; //!< Buffer for lines, of length num
	int prog_return_val = 0; //!< What should this program return

	//Parse arguments
	parse_param(argv, &num, &filename);
	//Skip doing anything if num == 0
	if (num == 0) {
		return 0;
	}

	//Open fd
	if (filename != NULL) {
		fd = open(filename, O_RDONLY);
		if (fd == -1) {
			// Error occured, write some error.
			if(write_str(2, "File open error: ", NULL) || !write_str(2, strerror(errno), NULL)) {
				write_str(2, "\n", NULL);
			}
			return 1;
		}
	}

	//Allocate space for line buffer
	buf = malloc(sizeof(line_t) * num);
	if (buf == NULL) {
		write_str(2, "Could not allocate memory\n", NULL);
		return 1;
	}
	for (size_t i = 0; i < num; i++) {
		// So you can't free erroneous pointers.
		buf[i].line = NULL;
	}

	while (1) {
		//Until read returns EOF
		line_t next = {.line = NULL, .len = 0, .allocated = 0};
		int val = nextline(fd, delim, &next);
		if (val == -1) {
			if(write_str(2, "Read error: ", NULL) || !write_str(2, strerror(errno), NULL)) {
				write_str(2, "\n", NULL);
			}
			// Doesn't say what do do on an error, so I'm just going to treat
			// it as EOF, and write the last lines.
			prog_return_val = 1;
			break;
		}
		if (next.len == 0) {
			//reached EOF.
			free(next.line);
			break;
		}

		//shuffle all forward by one.
		if (buf[0].line) {
			free(buf[0].line);
		}
		if (num != 1) {
			for (size_t i = 1; i < num; i++) {
				buf[i-1] = buf[i];
			}
			buf[num - 1] = next;
		}
		else {
			// Don't need to move nothing if we only have one line
			buf[0] = next;
		}
	}
	//Write out last lines.
	for (size_t i = 0; i < num; i++) {
		if (buf[i].line == NULL) {
			continue;
		}
		if (retry_write(1, buf[i].line, buf[i].len, NULL)) {
			// Write failed.
			if(write_str(2, "Write error: ", NULL) || !write_str(2, strerror(errno), NULL)) {
				write_str(2, "\n", NULL);
			}
			prog_return_val = 1;
			break;
		}
	}

	//free buf
	for (size_t i = 0; i < num; i++) {
		if (buf[i].line)
			free(buf[i].line);
	}
	free(buf);

	//Close fd
	if (fd != 0) {
		if (close(fd) == -1) {
			//Some error occured, write it out.
			//The fd is released anyway, even on error.
			if(write_str(2, "File close error: ", NULL) || !write_str(2, strerror(errno), NULL)) {
				write_str(2, "\n", NULL);
			}
		}
	}

	return prog_return_val;
}


// FUNCTION DEFINITIONS

int
nextline(int fd, const char delim, line_t *line) {
	static char buf[BUFFER_SIZE]; //!< Buffered input, saved across calls.
	static char *end = buf; //!< buf + BUFFER_SIZE. Points to after end.
	static char *begin = buf; //!< Points to next character.

	//Perform allocations if not done.
	if (line == NULL) {
		//Uuuh, you shouldn't do this.
		return -1;
	}
	else if (line->line == NULL || line->allocated == 0) {
		line->len = 0;
		line->line = malloc(sizeof(char) * BUFFER_SIZE);
		if (line->line == NULL) {
			// Out of memory.
			line->allocated = 0;
			return -1;
		}
		line->allocated = BUFFER_SIZE;
	}


	// Until delim is reached, keep reading.
	while (1) {
		// no data in buf?
		if (begin == end) {
			ssize_t chars_read;
			//If necesary, read more characters and update begin and end accordingly
			chars_read = read(fd, buf, BUFFER_SIZE);
			if (chars_read == -1) {
				if (errno == EINTR) {
					//Interrupted, retry.
					continue;
				}
				return -1;
			}
			else if (chars_read == 0) {
				// Stop! We have reached EOF!
				return 0;
			}
			end = buf + chars_read;
			begin = buf;
		}

		// no more space allocated?
		if (line->len == line->allocated) {
			// Double the space allocated.
			void *ptr = realloc(line->line, line->allocated * 2);
			if (ptr == NULL) {
				// Out of memory. errno is ENOMEM
				return -1;
			}
			line->line = ptr;
			line->allocated *= 2;
		}

		for (size_t i = (line->allocated - line->len > end - begin) ?
		end - begin : line->allocated - line->len; i; i--) {
			// While line still has space allocated
			// and there are characters buffered from read
			// Copy until reach delim
			line->line[line->len++] = *begin;
			if (*(begin++) == delim) {
				//Stop! We've copied the delimiter!
				return 0;
			}
		}
	}
}

void
freeline(line_t *line) {
	if (line == NULL) {
		return;
	}
	free(line->line);
	free(line);
}
