// Eli Faso-Formoso
// 2020-06-31
// head.c
// For CSCE A321 - Operating Systems

#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h> //for strerror only

#include "shared.h"

#define BUFFER_SIZE 256

//! Number of characters until (not including) the next
//! Character was not found if return == len
inline size_t
chars_until(char *buf, size_t len, char ch);

int
main(int argc, char const *argv[]) {
	size_t num = 10; //!< Number of lines left to read
	int fd = 0; //!< Where to read from
	char buf[BUFFER_SIZE]; //!< Character buffer for lines
	size_t char_remain = 0; //!< Characters remaining in the buffer
	int prog_return_val = 0; //!< What should this program return
	const char *filename = NULL; //!< The file to open and read from
	char delim = '\n'; //!< The delimiter. `man head` says may be \0

	//Parse arguments
	parse_param(argv, &num, &filename);
	//Skip doing anything if num == 0
	if (num == 0) {
		return 0;
	}

	//Open fd
	if (filename != NULL) {
		fd = open(filename, O_RDONLY);
		if (fd == -1) {
			// Error occured, write some error.
			if(write_str(2, "File open error: ", NULL) || !write_str(2, strerror(errno), NULL)) {
				write_str(2, "\n", NULL);
			}
			return 1;
		}
	}

	while (num != 0) {
		ssize_t read_return; //!< Return value from read/write

		// Refill buf
		// Buffer should be aligned to the left
		// Should never reach here with a full buffer
		read_return = read(fd, buf + char_remain, BUFFER_SIZE - char_remain);
		if (read_return == -1) {
			if (errno == EINTR) {
				// Interrupted by a signal, retry.
				continue;
			}
			if(write_str(2, "Read error: ", NULL) || !write_str(2, strerror(errno), NULL)) {
				write_str(2, "\n", NULL);
			}
			prog_return_val = 1;
			break;
		}
		char_remain += read_return;
		if (char_remain == 0) {
			//EOF reached, entire file has been read through.
			break;
		}

		// Find next newline
		size_t linelen = chars_until(buf, char_remain, delim);

		int returnval;
		if (char_remain != linelen) {
			//Found delim, we should write that out
			returnval = retry_write(1, buf, linelen + 1, NULL);
			num--;
			// Skip linelen and one more (delim)
			char_remain -= (linelen + 1);
			char *from = buf + linelen + 1;
			char *to = buf;
			for(size_t n = char_remain; n; from++, to++, n--) {
				*to = *from;
			}
		}
		else {
			//Haven't found delim, so just write out the whole buffer read.
			returnval = retry_write(1, buf, linelen, NULL);
			char_remain = 0;
		}
		if (returnval == -1) {
			//failure, write error message.
			//No functions that could change errno have been called since write
			if(write_str(2, "Write error: ", NULL) || !write_str(2, strerror(errno), NULL)) {
				write_str(2, "\n", NULL);
			}			prog_return_val = 1;
			break;
		}
	} // while loop

	//Close fd
	if (fd != 0) {
		if (close(fd) == -1) {
			//Some error occured, write it out.
			if(write_str(2, "File close error: ", NULL) || !write_str(2, strerror(errno), NULL)) {
				write_str(2, "\n", NULL);
			}		}
	}

	return prog_return_val;
}

size_t
chars_until(char *buf, size_t len, char ch) {
	size_t ret = 0;
	char *ptr = buf;
	for (; *ptr != ch && ret < len; ptr++, ret++);
	// If ptr == ch, then don't increment ret again.
	// If ret == len, then none of the characters were ch
	return ret;
}
